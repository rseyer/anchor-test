FROM obregon66/baufest:chrome-maven-jdk

USER root

RUN wget http://mirror.nbtelecom.com.br/apache//jmeter/binaries/apache-jmeter-5.2.1.tgz
RUN tar -xvzf apache-jmeter-5.2.1.tgz
RUN rm apache-jmeter-5.2.1.tgz

RUN mv apache-jmeter-5.2.1 /jmeter
WORKDIR /jmeter
RUN wget https://jmeter-plugins.org/files/packages/jpgc-perfmon-2.1.zip
RUN unzip -o jpgc-perfmon-2.1.zip
RUN wget https://jmeter-plugins.org/files/packages/jpgc-webdriver-3.1.zip
RUN unzip -o jpgc-webdriver-3.1.zip
ENV JMETER_HOME /jmeter

# Add Jmeter to the Path
ENV PATH $JMETER_HOME/bin:$PATH
WORKDIR /